package br.com.galgo.incluir;

import br.com.galgo.testes.recursos_comuns.file.entidades.Teste;

public interface Incluir {

	public void incluir(Teste teste);

}