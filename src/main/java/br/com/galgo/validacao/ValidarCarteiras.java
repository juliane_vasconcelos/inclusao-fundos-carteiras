package br.com.galgo.validacao;

import org.junit.Assert;

import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaValidacaoInclusaoCarteira;

public class ValidarCarteiras {

	public static void validarInclusao(String cnpj, String url) {

		TelaLogin telaLogin = new TelaLogin();
		TelaHome telaHome = telaLogin.loginAs(new Usuario(
				UsuarioConfig.AUTOREG_HOMOLOG));
		TelaValidacaoInclusaoCarteira telaValidacaoInclusaoCarteira = (TelaValidacaoInclusaoCarteira) telaHome
				.acessarSubMenu(SubMenu.VALIDACAO_INCLUSAO_CARTEIRAS);
		telaValidacaoInclusaoCarteira
				.clicarValidacaoInclusaoRegistro(telaValidacaoInclusaoCarteira);
		Assert.assertTrue("Carteira não incluído com sucesso",
				telaValidacaoInclusaoCarteira.verificaTextoNaTela(cnpj));
		telaLogin.logout();

	}
}
